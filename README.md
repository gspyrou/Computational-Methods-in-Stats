# Computational-Methods-in-Stats
Projects for the courses STATS 506 - Computational Methods and Tools in Statistics

1) Data Analysis project concerning the Australian Shipping data ( https://www.operations.amsa.gov.au/Spatial/DataServices/DigitalData)

**Relative file: Australian_shipping.r**

For this project, I used the programming language R in order to conduct Exploratory Data Analysis on a large dataset (more than 10million rows) containing information about vessels which are travelling around the Australian Area, and attempt to extract useful information by applying many different statistical analysis techniques. The main task was to use Descriptive Statistics and Visualization (ggplot2) and answer questions concerning the impact that each country has at the marine area around Australia, something that we measured through different statistics (average length of the ships, comparison of tonnage weights, etc).Finally, we took advantage of the Longitude and Latitude attributes of the dataset and built a spatial representation of our data depicting where the vessels were located in the map, while taking into consideration some time constraints.

2) Descriptive Analysis on Medical data (Medicare) ( https://umich.app.box.com/s/h8hizh4nmq8wkk4e23x1nvtnxt8eanfu)

**Relative File: medicare_analysis.sas **


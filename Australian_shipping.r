
# Author : Georgios Spyrou / University of Michigan, Ann Arbor / Dept.of Statistics

# Please if you would like to run the code, you will have to follow this link:
# https://www.operations.amsa.gov.au/Spatial/DataServices/DigitalData
# Then go at year 2011, download the .zip file and extract and retrieve the "ausrep_2011.dbf" file
# which contains our data

# Installing and loading the required libraries for our analysis
# Then we have to perform a pre-analysis/cleaning and check for any
# issues (like missing values,etc)

librar_list = c("foreign","dplyr","tidyr","data.table","ggplot2","ggthemes")
install.packages(librar_list)
lapply(librar_list, require, character.only = TRUE)

file = "C:\\Users\\george\\Documents\\ausrep_2011.dbf" # add your filepath here
data = as.data.frame(read.dbf(file)) 

row.has.na = apply(data, 1, function(x){any(is.na(x))})
sum(row.has.na) 
clean_data =  data[!row.has.na,]

# Function that returns the first-obs_num and last-obs_num observations given a table
show_tables = function(the_table,obs_num){
  table_dim = dim(the_table)

  print(the_table[1:obs_num,])
  print(the_table[(dim(the_table)[1]-obs_num+1):dim(the_table)[1],])
  
}

# Question 1 / Part A
# Calculating the average length of the ships for each country of origin
# and then present ,as a table, the 5 biggest and 5 smallest averages

avg_len_per_country = clean_data%>%select(CNTRY_CODE,SHIPLENGTH)%>%group_by(CNTRY_CODE)%>%
                                            summarize('AverageLength'=mean(SHIPLENGTH))%>%
                                            spread(CNTRY_CODE,'AverageLength')

data_long = as.data.frame(gather(avg_len_per_country, 
                                        CNTRY_CODE, 'AverageLength', factor_key=TRUE))

data_long = cbind(data_long[1],round(data_long[2],digits=2))
colnames(data_long)[2]='Average Length of Ships'

data_long = arrange(data_long,desc(as.double(`Average Length of Ships`)))

cntry_averages_overall = subset(data_long,data_long$`Average Length of Ships`>0) 

show_tables(cntry_averages_overall,5)

# Part B - Calculating  the standard deviations, the log-standard deviations
# and frequencies for the length of the ships, grouped by country
# Then we present the 3 max and 3 min observations.

shiplen_SD_logSD = as.data.frame(clean_data)%>%select(CNTRY_CODE,SHIPLENGTH)%>%
              group_by(CNTRY_CODE)%>%summarise('StandardDeviation'= sd(SHIPLENGTH),
                                               'LogStandardDeviation'=(log(StandardDeviation)),
                                                n = n())%>%arrange(CNTRY_CODE)

# Removing the observation having 0's or NA's.
# In case of 0's it means that we have the same value for all the ships for the specific country
shiplen_SD_logSD = shiplen_SD_logSD[-which(is.na(shiplen_SD_logSD$StandardDeviation)),]
shiplen_SD_logSD = shiplen_SD_logSD[-which(shiplen_SD_logSD$StandardDeviation==0),]

final_shiplen_table = as.data.frame(inner_join(shiplen_SD_logSD,cntry_averages_overall,by="CNTRY_CODE"))%>%
                                                     arrange(desc(as.double(StandardDeviation)))

show_tables(final_shiplen_table,3)

# Now,we build the confidence intervals for the aforementined analysis(on SD's)
# considering again the ship lenghts and the specific countries of origin

confinterv = final_shiplen_table%>%group_by(CNTRY_CODE)%>%
            summarise("Standard Error"=StandardDeviation/sqrt(n)) 

full_table = inner_join(final_shiplen_table,confinterv,by="CNTRY_CODE") 

lowerbound = as.matrix(full_table$`Average Length of Ships` - 2*full_table$`Standard Error`)   
upperbound = as.matrix(full_table$`Average Length of Ships` + 2*full_table$`Standard Error`)   

mergedtable = cbind(full_table,lowerbound,upperbound)

# Therefore we can visualize the upper and lower bounds, around their mean
# We will do it for the first 5 countries with the highest Standard Errors

pick_tb =  mergedtable[order(desc(mergedtable$`Standard Error`)),][1:5,]

ggplot(pick_tb,aes(x = CNTRY_CODE,y =`Average Length of Ships`))+geom_point(size=1.5)+
    geom_errorbar(width=.15,size=0.7, aes(ymin=lowerbound, ymax=upperbound))+
    labs(title= " Bounds around their mean for 5 highest SE",x = "Country")+
    theme_economist_white()+theme(plot.title = element_text(color="gray38",size = 15),
                                  axis.text = element_text(size = 8))
    
# Question 2

# Lets try and find the geographical centroid of the ships
# The expectation is that the overall centroid of all the ships 
# should be somewhere around  the middle of Australia

longlat_table = clean_data%>%select(VOYAGE_ID,LONGITUDE,LATITUDE)%>%
  group_by(VOYAGE_ID)%>%summarise("Lati"=mean(LATITUDE),"Longi"=mean(LONGITUDE))  

# proper transformations for x,y,z-('spherical'-'cartesian')
longlat_table$Lat = longlat_table$Lati*pi/180        
longlat_table$Long = longlat_table$Longi*pi/180

longlat_table$x  =  3959*cos(longlat_table$Lat)*cos(longlat_table$Long)       
longlat_table$y  =  3959*cos(longlat_table$Lat)*sin(longlat_table$Long)
longlat_table$z  =  3959*sin(longlat_table$Lat)

cartes_means = apply(longlat_table,2,mean)[6:8]%>%as.data.frame()
cartes_means = tibble::rownames_to_column(cartes_means,"Cartesian Point")
colnames(cartes_means)[2] = c("Value_Overall")

print(cartes_means)

# If we search at the map considering the x,y,z values we can see that 
# the centroid is almost exactly at the middle of Australia and it makes sense considering that our data
# corresponds to ships that are moving all around Australia (in a "circle")

# Now we can split the data and calculate the centroid for two cases: 
# One case for voyage ID's that their average difference between the date that entered the 
# reported zone and their exit date is less than 30 days,while the second case is for 
# a difference over 30 days(ships stayed in the area more than a month)

longlat_reduced = clean_data%>%select(VOYAGE_ID,LONGITUDE,LATITUDE,ENTER_DATE,EXIT_DATE)

longlat_reduced$DifferenceinDays = longlat_reduced$EXIT_DATE - longlat_reduced$ENTER_DATE

avr_days_zone = subset(longlat_reduced,DifferenceinDays > 0)%>%group_by(VOYAGE_ID)%>%
                          summarise("AverageDaysInTheZone"=mean(DifferenceinDays))%>%
                          inner_join(longlat_table,by="VOYAGE_ID")  

over30 = as.data.frame(subset(avr_days_zone,AverageDaysInTheZone>30))
under30 = as.data.frame(subset(avr_days_zone,AverageDaysInTheZone<=30))

all_list  = lapply(list(over30,under30), function(x) colMeans(subset(x, select = c("x", "y","z"))))%>%as.data.frame()

colnames(all_list) = c("Value_over30","Value_under_30")

final_table_cartesians = cbind(cartes_means,all_list)%>%as.data.frame()

print(final_table_cartesians,row.names = FALSE)

# Thus if we look again at the real coordinates we will see that the ships which stayed at the “research “territory and they 
# had to make reports for over a month,seem to sail mostly at the left side of the Australian area, 
# while the ships that are not staying for a long time (under a month) follow approximately the overall case of the centroid 

# End of the file  / Author: Georgios Spyrou / University of Michigan

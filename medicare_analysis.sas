
/* question 1 */
filename blue pipe "gzip -dc Medicare_Provider_Util_Payment_PUF_CY2013_subset.txt.gz" ;   /* loading the proper txt file */
data test;   /* we are going to use a data step  */
  infile blue delimiter="09"x dsd firstobs=3;    /* setting the delimiter as long as we are starting from the 3rd obs because the first two rows are irrelevant */
  input npi nppes_credentials $ nppes_provider_gender $ nppes_entity_code $ nppes_provider_zip nppes_provider_state $ provider_type $ medicare_participation_indicator $ place_of_service $ hcpcs_code hcpcs_drug_indicator $ line_srvc_cnt bene_unique_cnt bene_day_srvc_cnt average_Medicare_allowed_amt stdev_Medicare_allowed_amt average_submitted_chrg_amt stdev_submitted_chrg_amt average_Medicare_payment_amt stdev_Medicare_payment_amt ;
   
/* now we have all the proper variables for our dataset */

proc print data=test(obs=10) ;  /* we are printing the first 10 observations */
proc contents data=test;        /* contents procedure- we see that we are ok */
run;
/* question 2 */
data test;          /* next data step */
  set test;
  amount=line_srvc_cnt;       /* we will use as our amount of interest the variable line_srvc_cnt */

proc sort data=test out=test1;
  by nppes_provider_state nppes_provider_gender ;         /* helping our data to sort by the variables we want- gender,state */

proc freq data=test1 noprint;                            /* freq procedure  by gender and state */
  by nppes_provider_state nppes_provider_gender ; 
  tables  amount*nppes_provider_state*nppes_provider_gender / list noprint out = newlist ;   /* formatting the proper table - i used noprint because the output was huge */
run;
proc print data=newlist;
proc means data=newlist max min;         /* means procedure so we can see the Number of observations as long as Max and Min values */
  class nppes_provider_gender nppes_provider_state;   /* by gender, state */
  var amount;    /* variable of interest in which will will focus for counting is amount as defined at part 1 */
run;

/* question 3 */

/* here we are focusing only to single record for each provider and then repeat the operation we have done at part 2 */
proc sort data=test out=test2;
  by npi;
data test2;
  set test2;
  by npi;            /* after sorting by provider we will try and "extract" only one single record for each provider */
  first=first.npi;   /* we are using ther first. statement so i will assign the value 1 at the first obs of every provider and 0 to every next obs for this provider */
  if first ~= 1 then delete;   /* subsetting the data so it can contain only the proper variables , i.e. the ones with value 1 at "first" . Every obs which has "first" equal to 0 is dropped */
data test2;
  set test2;
  amount=line_srvc_cnt;       /*the procedure now is nothing special , as we repeating the same steps and logic as part b */
proc sort data=test2 out=test3;
  by nppes_provider_state nppes_provider_gender;
proc freq data=test3;
  by nppes_provider_state nppes_provider_gender;
  tables amount*nppes_provider_state*nppes_provider_gender / list nonprint out=newlist2;
run;
proc print data=newlist2;
proc means data=newlist2 max min;
  class nppes_provider_gender nppes_provider_state;
  var amount;    
run;

/* end of problem set 2 */
